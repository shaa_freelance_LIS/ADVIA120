#-------------------------------------------------
#
# Project created by QtCreator 2015-05-30T09:36:06
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets serialport

lessThan(QT_MAJOR_VERSION, 5): CONFIG += serialport

TARGET = ADVIA120
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    addons/c_assert.cpp \
    LIS/lowlevel/lis.c \
    LIS/lowlevel/lis_receiver.c \
    LIS/TLisInterface.cpp \
    LIS/TLisInterfaceCom.cpp \
    LIS/TLisMode.cpp \
    Connector.cpp \
    LIS/TLisFile.cpp

HEADERS  += mainwindow.h \
    addons/lis_cfg.h \
    addons/c_assert.h \
    LIS/lowlevel/lis.h \
    LIS/lowlevel/lis_codetable.h \
    LIS/lowlevel/lis_receiver.h \
    LIS/TLisInterface.h \
    LIS/TLisInterfaceCom.h \
    LIS/TLisMode.h \
    Connector.h \
    LIS/TLisFile.h


FORMS    += mainwindow.ui \
    Connector.ui

RESOURCES += \
    resources.qrc

INCLUDEPATH += $$PWD/addons
