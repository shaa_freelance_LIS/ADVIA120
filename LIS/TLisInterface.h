/*!
 * \file TLisInterface.h
 * \author Андрей Шиганцов
 * \brief Заголовочнй файл базового класса интерфейса для протокола LIS
 */

#ifndef TLISINTERFACE_H
#define TLISINTERFACE_H

#include <QObject>
#include <QTimer>
#include "lowlevel/lis.h"
#include "lowlevel/lis_receiver.h"

/*! Базовый класс интерфейса для протокола LIS */
class TLisInterface : public QObject
{
    Q_OBJECT
public:
    enum ConnectStatus {Disconnected, Connected};
    enum OperationStatus {Ready, Waiting, Error = -1};
    enum OperationResult {Success = 0, Failure = -1, nack_MaxCountExceeded = -0x10};

    explicit TLisInterface(QString Name, QObject *parent = 0);

    /*! Возвращает статус соединения */
    ConnectStatus getConnectStatus(){return cStatus;}
    /*! Возвращает статус операции */
    OperationStatus getOperationStatus(){return oStatus;}

    /*! Отправить запрос на инициализацию устройства */
    void send_init();
    /*! Отправить задание */
    void send_workorder(lis_WorkorderData_t *data);
    /*! Отправить токен */
    void send_token();
    /*! Отправить подтверждение результата */
    void send_resultvalidation(lis_ResultValidationData_t *data);

    /*! Отправить сообщение */
    bool send_pdu(lis_ProtocolDataUnit_t* pdu /*!< [in] Сообщение */, bool isCheckOperation = 1);

    /*! Полный сброс интерфейса */
    void reset();
    /*! Сброс состояния интерфейса */
    void resetState();
    /*! Сброс операционного статуса */
    inline void resetOperationStatus() { setOperationStatus(Ready); WatchdogTimer.stop(); }

    /*! Возвращает строку содержащую номер ошибки lis_ErrorCode_t и его описание */
    static inline QString errorString(lis_ErrorCode_t err){return QString("(%1) %2").arg(err).arg(lis_error_text((lis_ErrorCode_t)err));}
    /*! Возвращает строку содержащую номер ошибки lis_ErrorCode_t и его описание */
    /*! это перегруженная функция */
    static inline QString errorString(int err){return errorString((lis_ErrorCode_t)err);}

signals:
    /*! Состояние соединения изменилось */
    void ConnectStatus_changed(TLisInterface::ConnectStatus status /*!< [in] Новое состояние соединения */);
    /*! Состояние обраотчика операций изменилось */
    void OperationStatus_changed(TLisInterface::OperationStatus status /*!< [in] Новое состояние последней операции */);

    /*! Результат операции */
    void OperationResult_ready(TLisInterface::OperationResult res /*!< [in] код результатата */);

    /*! Сообщение отправлено */
    void pdu_sent(lis_ProtocolDataUnit_t pdu /*!< [in] Сообщение */);
    /*! Принято соощение */
    void pdu_received(lis_ProtocolDataUnit_t pdu /*!< [in] Сообщение */);

    /*! Cообщение об ошибке */
    void error(QString src /*!< [in] Иточник ошибки */,
               QString msg /*!< [in] Сообщение */);

protected:
    QTimer WatchdogTimer;
    int WatchdogTimeout;

    /*! Установить статус соединения */
    void setConnectStatus(ConnectStatus status);
    /*! Установить статус обработчика операций */
    void setOperationStatus(OperationStatus status);

    /*! Отравить данные */
    virtual bool send(LIS_DATA data) = 0;
    /*! Отправить буфер */
    virtual bool send(LIS_DATA* buf, LIS_SIZE size) = 0;

    /*! Обработчик приходящих данных*/
    inline void read_buf(LIS_DATA* buf, LIS_SIZE size)
    {
        int res = lis_receiver_read_buf(&receiver, buf, size);
        if (res < 0)
            send_error("Read buf", TLisInterface::errorString(res));
    }

    /*! Отправить сообщение об ошибке */
    void send_error(QString src /*!< [in] Иточник ошибки */,
                    QString msg /*!< [in] Сообщение */);

private:
    QString Name;

    ConnectStatus cStatus;
    OperationStatus oStatus;

    lis_ProtocolDataUnit_t pdu;
    LIS_DATA currentMT;
    int maxNackCount, nackCounter;

    /*! Проверка статуса операции */
    bool check_operation_status();
    /*! Получить новый MT */
    LIS_DATA getMT();

    /*! Приёмник сообщений */
    lisReceiver_t receiver;

    /*! Обработчик принятого сообщения (обратный вызов для Приёмника) */
    static void receiver_pdu_ready(void* This, LIS_DATA* buf, LIS_SIZE size);
    /*! Обработчик принятого MT байта (обратный вызов для Приёмника) */
    static void receiver_mt_ready (void* This, LIS_DATA mt);

    /*! Обработчик NACK */
    void nack_proc();
    /*! Сброс NACK */
    void nack_reset(){nackCounter = 0;}

private slots:
    /*! Обработчик Watchdog */
    void watchdog_timeout_handler();
};

#endif // TLISINTERFACE_H
