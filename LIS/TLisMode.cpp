/*!
 * \file TLisDownloadingMode.cpp
 * \author Андрей Шиганцов
 * \brief Исходные коды класса реализации Downloading workorder mode протокола LIS
 */

#include "TLisMode.h"

TLisMode::TLisMode(TLisInterface *interface, Type mode, QObject *parent) :
    QObject(parent)
{
    ifX = interface;
    Mode = mode;
    isStop = true;

    connect(ifX, SIGNAL(OperationResult_ready(TLisInterface::OperationResult)), this, SLOT(OperationResult_handler(TLisInterface::OperationResult)));
    connect(ifX, SIGNAL(pdu_received(lis_ProtocolDataUnit_t)), this, SLOT(pdu_handler(lis_ProtocolDataUnit_t)));
}

TLisMode::~TLisMode()
{
    disconnect(ifX, SIGNAL(OperationResult_ready(TLisInterface::OperationResult)), this, SLOT(OperationResult_handler(TLisInterface::OperationResult)));
    disconnect(ifX, SIGNAL(pdu_received(lis_ProtocolDataUnit_t)), this, SLOT(pdu_handler(lis_ProtocolDataUnit_t)));
    ifX->reset();
}

QString TLisMode::TypeName(TLisMode::Type type)
{
    switch(type)
    {
    default: return "";
    case DownloadingMode: return "mode D";
    case QueryMode: return "mode Q";
    case FullQueryMode: return "mode F";
    }
}

void TLisMode::start()
{
    isStop = false;
    init_device();
}

void TLisMode::stop()
{
    isStop = true;
    if (phase != ResultsTransmission)
        reset();
}

void TLisMode::reset()
{
    phase = Idle;
    ifX->reset();
    Workorders.clear();
    WorkordersId.clear();
    currentWorkorderId = "";
}

void TLisMode::init_device()
{
    set_phase(Initialisation, tr("инициализация..."));

    ifX->send_init();
}

void TLisMode::reinit_device()
{
    set_phase(Initialisation, tr("реинициализация..."));

    ifX->reset();
    ifX->send_init();
}

int TLisMode::transmit_workorder()
{
    if (Workorders.isEmpty()) return -1;
    lis_WorkorderData_t w = Workorders.first();

    set_phase(WorkorderTransmission, tr("передача задания SampleID=%1...").arg(w.SampleId));

    ifX->send_workorder(&w);
    Workorders.removeFirst();

    currentWorkorderId = WorkordersId.first();
    WorkordersId.removeFirst();

    return Workorders.count();
}

void TLisMode::transmit_token()
{
    set_phase(TokenTransfer, tr("передача токена..."));

    ifX->send_token();
}

void TLisMode::transmit_resultvalidation()
{
    QString text;
    lis_ResultValidationData_t data;
    if (isStop)
    {
        text = tr("передача команды остановки...");
        data.Error = lis_resvalerr_Stop;
    }
    else
    {
        text = tr("передача подтверждения результата...");
        data.Error = lis_resvalerr_No;
    }

    set_phase(ResultValidationTransfer, text);

    ifX->send_resultvalidation(&data);
}

void TLisMode::send_warning(QString src, QString msg)
{
    emit warning(TypeName(Mode) + ": " + src, msg);
}

void TLisMode::send_error(QString src, QString msg)
{
    emit error(TypeName(Mode) + ": " + src, msg);
}

void TLisMode::set_phase(TLisMode::Phase phase, QString text)
{
    this->phase = phase;
    emit message(text);
}

void TLisMode::success_handler_d()
{
    switch(phase)
    {
    default:
        break;

    case Initialisation:
        if (transmit_workorder() < 0)
            transmit_token();
        break;

    case ResultValidationTransfer:
        if(isStop)
        {
            reset();
            break;
        }
    case TokenTransfer:
        set_phase(ResultsTransmission, tr("ожидание результатов..."));
        break;
    }
}

void TLisMode::pdu_handler_d(lis_ProtocolDataUnit_t* pdu)
{
    QString text;
    switch(phase)
    {
    default:
        break;

    case WorkorderTransmission:
        switch (pdu->id)
        {
        default: break;

        case lisid_WorkorderValidation:
            if (pdu->data.WorkorderValidation.Mode != lis_wrkvalmode_D)
                send_warning(tr("Интерфейс"), tr("устройство находиться в режиме Q"));

            emit workorder_transmitted(currentWorkorderId, pdu->data.WorkorderValidation.Error == lis_wrkvalerr_No);
            currentWorkorderId = "";

            switch(pdu->data.WorkorderValidation.Error)
            {
            case lis_wrkvalerr_No:
                text = tr("задание принято на выполнение");
                break;

            case lis_wrkvalerr_Yes:
                text = tr("устройство не поддерживает заданный тест");
                break;
            }
            emit message(text);

            if (transmit_workorder() == -1)
                transmit_token();
            return;
        }
        break;

    case ResultsTransmission:
        switch (pdu->id)
        {
        default: break;

        case lisid_Result:
            emit pdu_ready(*pdu);
            emit message(tr("принят результат SampleID=%1").arg(pdu->data.Result.SampleId));
            transmit_resultvalidation();
            return;

        case lisid_TokenTransfer:
            emit message(tr("принят токен"));
            if (transmit_workorder() == -1)
                transmit_token();
            return;
        }
        break;
    }
    text = tr("принято сообщение id=%1").arg(lis_id_char(pdu->id));
    emit message(text);
}

void TLisMode::OperationResult_handler(TLisInterface::OperationResult res)
{
    switch(res)
    {
    case TLisInterface::Success:
        switch (Mode)
        {
        case DownloadingMode:
            success_handler_d();
            break;
        case QueryMode:
            break;
        case FullQueryMode:
            break;
        }
        break;

    case TLisInterface::nack_MaxCountExceeded:
    case TLisInterface::Failure:
        reinit_device();
        break;
    }
}

void TLisMode::pdu_handler(lis_ProtocolDataUnit_t pdu)
{
    switch (Mode)
    {
    case DownloadingMode:
        pdu_handler_d(&pdu);
        break;
    case QueryMode:
        break;
    case FullQueryMode:
        break;
    }
}

