/*!
 * \file lis_receiver.h
 * \author Андрей Шиганцов
 * \brief Заголовочный файл приёмника сообщений по протоколу LIS
 */

#ifndef __LIS_RECEIVER_H__
#define __LIS_RECEIVER_H__

#include "lis.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef void (*lis_buffer_ready_callback_t)(void* parent, LIS_DATA* buf, LIS_SIZE size);
typedef void (*lis_data_ready_callback_t)(void* parent, LIS_DATA data);

/*! Настройки приёмника сообщений по протоколу LIS */
typedef struct
{
    void* Parent; /*!< указатель на родителя */
    lis_buffer_ready_callback_t pdu_ready; /*!< функция обработчик принятого сообщения */
    lis_data_ready_callback_t mt_ready; /*!< функция обраотчик принятого MT(Message Toggle code) */
} lisReceiverCfg_t;

/*! Состояние приёмника сообщений по протоколу LIS */
typedef enum
{
    lisrstate_Waiting,
    lisrstate_Receiving
} lisReceiverState_t;

/*! Структура данных приёмника сообщений по протоколу LIS */
typedef struct
{
    lisReceiverState_t State;

    LIS_DATA buf[LIS_MAX_BUFSIZE];
    LIS_SIZE Counter;

    void* Parent;
    lis_buffer_ready_callback_t pdu_ready;
    lis_data_ready_callback_t mt_ready;
} lisReceiver_t;

/*! Инициализация приёмника сообщений по протоколу LIS */
void init_lisReceiver(lisReceiver_t* This, lisReceiverCfg_t* cfg);
/*! Сброс состояния приёмника сообщений по протоколу LIS */
void reset_lisReceiver(lisReceiver_t* This);

/*! Побайтный приём сообщения по протоколу LIS */
int lis_receiver_read_byte(lisReceiver_t *This, LIS_DATA byte);

/*! Буферный приём сообщений по протоколу LIS */
int lis_receiver_read_buf(lisReceiver_t *This, LIS_DATA* buf, LIS_SIZE size);

#ifdef __cplusplus
}
#endif

#endif//__LIS_RECEIVER_H__
